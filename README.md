# Bot #

This is an IRC bot that I started developing somewhere around 2014-2015. As IRC started to get less and less relevant, I decided to put all development to rest in 2017.

The bot gets all settings and joins channels either from a config file or command line. My plan was to introduce threading and handling user commands, but it made me hit a mental roadblock.
