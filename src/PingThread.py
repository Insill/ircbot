#!/usr/bin/env python3
# encoding -*- utf-8 -*-
''' A class that takes care of ping. A thread is started when the bot is idle and it checks if the server has posted PING. It gets answered by PONG.'''

import logging
import time

logger = logging.getLogger(__name__)


class PingThread:

	def __init__(self, bot):
		self.bot = bot

	def run(self):
		last_ping = time.time()
		threshold = 5 * 60

		while(self.bot.connected):

			self.ping(self.bot)
			last_ping = time.time()

			time.sleep(60)

			if (time.time() - last_ping) > threshold:
				logger.debug("No response!")
				break

	def ping(self):
		self.bot.send("PONG :PONG")
