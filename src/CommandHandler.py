#!/usr/bin/env python3
# encoding -*- utf-8 -*-
''' A class that takes care of handling commands, called when a command with the dollar symbol is detected 
TODO: Make this a thread
@author Insill
'''
import configparser
import logging
import threading

parser = configparser.ConfigParser()
logger = logging.getLogger(__name__)


class CommandHandler:

	def __init__(self, bot):
		self.bot = bot

	def run(self, text):

		self.handle_command(self, text)

	def handle_command(self, text):
		user = text.split("!")[0].replace(':', ' ')
		command = text.split("$")[1]
		channel = self.get_channel(text)
		logger.debug("Command {0} from user {1} from {2}".format(
			command, user, channel))
		# TODO: Handling commands when the user gives a command with only one word
		if len(command.split()) >= 2:
			# It's useless to get channel from PRIVMSG messages
			self.chooseCommand(user, command, channel)
		elif len(command.split()) == 1:
			self.chooseCommand(user, command, channel)

	def choose_command(self, user, command, channel):
		commands = {"version": self.version,
                    "king": self.king,
                    "help": self.help,
                    "hello": self.hello,
                    "op": self.de_op,
                    "deop": self.give_op,
                    "join": self.join,
                    "part": self.part,
                    "quit": self.disconnect,
                    "kick": self.kick,
                    "np": self.lastfm,
                    "script": self.script
              }

		commands[command]()

	def lastfm(self, bot, channel):
		bot.send(
			("PRIVMSG {0}:LAST.FM FUNCTIONALITY UNDER CONSTRUCTION").format(channel))

	def version(self, bot, channel):
		bot.send(
			("PRIVMSG {0} :Insillin ihkaoma IRC-botti, versio 0.5".format(channel)))

	def king(self, bot, channel):
		bot.send(("PRIVMSG {0} :Insill is my king!\r\n".format(channel)))

	def send(self, bot, channel, message):
		bot.send(("PRIVMSG {0} : {1}".format(channel, message)))
		logger.info(("PRIVMSG {0} : {1}".format(channel, message)))

	def hello(self, bot, channel):
		bot.send(("PRIVMSG {0}:Howdy ho!".format(channel)))
		logger.info(("PRIVMSG {0}:Howdy ho!".format(channel)))

	def help(self, bot, channel):
		bot.send(
			(("PRIVMSG {0} :Commands: $king $hello $help $op $deop (user, channel) $quit $part.".format(channel))))
		logger.info(
			(("PRIVMSG {0} :Commands: $king $hello $help $op $deop (user, channel) $quit $part.".format(channel))))

	def slap(self, bot, user, channel):
		bot.send(
			"PRIVMSG {0}: CMND_CTRL slaps {1} with a large trout".format(channel, user))
		logger.info(
			"PRIVMSG {0}: CMND_CTRL slaps {1} with a large trout".format(channel, user))

	def disconnect(self, bot):
		logger.info("\n----------\nThe bot is quitting...\n----------\n")
		bot.disconnect()

	def get_op(self, op):
		op = op.split("@")[1]
		op = op.split(" ")[0]
		return op

	def give_op(self, bot, oppable, channel):
		bot.send("MODE {0} +o: {1}".format(channel, oppable))
		logger.info("MODE {0} +o: {1}".format(channel, oppable))

	def de_op(self, bot, deoppable, channel):
		bot.send("MODE {0} -o: {1}".format(channel, deoppable))
		logger.info("MODE {0} -o: {1}".format(channel, deoppable))

	def voice(self, bot, voicable, channel):
		bot.send("MODE {0} +v: {1}".format(channel, voicable))
		logger.info("MODE {0} +v: {1}".format(channel, voicable))

	def de_voice(self, bot, devoicable, channel):
		bot.send("MODE {0} -v {1}".format(channel, devoicable))
		logger.info("MODE {0} -v {1}".format(channel, devoicable))

	def read_admin(self, admin):
		parser.read("config.ini")
		line = parser.get("Admins", "admin")
		if admin in line:
			return True
		else:
			return False

	def get_channel(self, text):
		channel = text.split("#")[1]
		channel = channel.split(":")[0]
		channel = "#" + channel
		channel = channel.strip(" \t\n\r")
		return channel

	def find_channel(self):  # Reads channels to join from a text files
		parser.read("config.ini")
		channel = parser.get("Channels", "channel")
		return channel

	def join_channel(self, bot, channel):
		bot.send("JOIN {0}".format(channel))
		logger.info("JOIN {0}".format(channel))
		logger.info("\n----------\nJoined channel {0}\n----------\n".format(channel))

	def part_channel(self, bot, channel):
		bot.send("PART {0}\r\n".format(channel))
		logger.info("PART {0}\r\n".format(channel))
		logger.info("\n----------\nParted channel {0}\n----------\n".format(channel))

	def kick(self, bot, channel, nick, message):
		bot.send("KICK {0} {1} {2}".format(channel, nick, message))
		logger.debug("KICK {0} {1} {2}".format(channel, nick, message))
		logger.info(
			"\n----------\nKicked user {0} {1} {2}".format(nick, channel, message))

	def script(self, bot, file):
		logger.info("UNDER CONSTRUCTION")
