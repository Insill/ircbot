#!/usr/bin/env python3
# encoding -*- utf-8 -*-
'''
@author Insill
'''
import socket
import sys
import configparser
import os
import logging
import select
import threading
from CommandHandler import CommandHandler
from PingThread import PingThread
from time import sleep
from logging import config

# Using Python's own logging module is better for debugging purposes than
# printing
logging.config.fileConfig("logging.ini", disable_existing_loggers=False)
logger = logging.getLogger(__name__)


class Bot:

	# Creates a Bot object
	def __init__(self, nick, realname, server, hostname, port):
		self.nick = nick
		self.realname = realname
		self.server = server
		self.hostname = hostname
		self.port = port
		self.irc_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		self.irc_socket.settimeout(200)
		self.commands = CommandHandler(self)
		self.connected = False

#-------------------------------Methods taking care of connection-----------------------------------------------#
	def send(self, message):  # Equivalent of sending something with socket - with necessary formatting added, used also by other classes
		self.irc_socket.send(("{0}\r\n".format(message)).encode())

	def receive(self):  # For other classes in order to get text to parse
		text = self.irc_socket.recv(1024).decode().strip("\n\r")
		return text

	def disconnect(self):
		logger.info("\n----------\nThe bot is quitting...\n----------\n")
		self.irc_socket.send(("QUIT\r\n").encode())
		self.irc_socket.shutdown(socket.SHUT_RDWR)
		self.irc_socket.close()
		sys.exit()

	def connect(self):
		# Connect to IRC network and read the channels to join from a text file
		try:

			self.irc_socket.connect((self.server, self.port))
			logger.info("\n----------\n" + 
			            "Connecting to {0} {1}\n----------".format(self.server, str(self.port)))
			logger.info("\n----------\nIP Address: {0} Host: {1} \n----------".format(
				socket.gethostbyname(socket.gethostname()), socket.gethostname()))
			sleep(10)

			self.send("PASS 1234")
			self.send("NICK {0}".format(self.nick))
			self.send("USER {0} {1} {2}: {3}".format(
				self.nick, self.hostname, self.server, self.realname))
			text = self.receive()
			logger.info(text)
			sleep(10)

		except socket.timeout:
			e = sys.exc_info()
			logger.exception(
				"\n--------\nError while trying to connect: {}\n---------\n".format(e), exc_info=True)
			sys.exit()

		self.connected = True
		self.commands.join_channel(self, self.commands.find_channel())
		self.idle()

	def idle(self):

		logger.debug("Start PingThread")
		pingthread = PingThread(self)
		ping_t = threading.Thread(target=pingthread.run, args=(self,))
		ping_t.start()

		while self.connected:
			try:
				ready = select.select([self.irc_socket], [], [], 200)
				if ready[0]:
					text = self.receive()
					if text:
						logger.info(text)
				'''	if text.split("$")[1] != " ":	TODO: Make parsing a command work
							logger.debug("Start CommandHandler")
							c_handler = CommandHandler(self)						
							handler_c = threading.Thread(target=c_handler.run, args=(text))
							handler_c.start() 
				'''
			except:
				self.connected = False
				e = sys.exc_info()
				logger.exception(
					"\n--------\nError while idle: {}".format(e) + "\n--------", exc_info=True)
				self.disconnect()

#---------------------------------------------The main method which creates a new object of the Bot class -----------------------------------------------------#


def main():
	# Initializing the variables for configuration since Python throws an
	# UnBoundLocalError otherwise
	realname, nick, server = ("default",) * 3
	hostname = None
	port = 6667

	# Configuration is read from an INI file, otherwise it can be input from
	# command line
	if os.path.isfile("config.ini"):
		parser = configparser.ConfigParser()
		parser.read("config.ini")
		server = parser.get("Configuration", "Server")
		port = int(parser.get("Configuration", "Port"))
		hostname = socket.gethostbyname(socket.gethostname())
		realname = parser.get("Configuration", "RealName")
		nick = parser.get("Configuration", "Nick")

	elif len(sys.argv) >= 6:
		nick = sys.argv[0]
		realname = sys.argv[1]
		server = sys.argv[2]
		hostname = sys.argv[3]
		port = int(sys.argv[4])

	else:  # Without any arguments, this is printed
		print("Usage: bot.py [nick][realname][server][hostname][port]")
		sys.exit()

	Bot(nick, realname, server, hostname, port).connect()


main()
